<?php
error_reporting(0);

require 'login.class.php'; // Einbinden des Login-Systems

$perm = new system(); // Class einer Variable zuweisen
$perm->setMYSQL("localhost", "root", "", "login"); // Host, User, Passwort, Datenbank
$perm->getData(); // Alle Informationen aus Datenbank lesen und in Variablen speichern

$perm->var = "Login"; // Setzt die Main-page

if($perm->login_check()){

	$perm->session_check("Projects"); // Überprüft ob der Nutzer bereits angemeldet ist

	if(isset($_SESSION['current'])){
		$perm->var = $_SESSION['current']; // Aktuelle Seite
	}

	switch($_GET['action']){

		case 'logout':

			$perm->logout();
			header("Location: /");

		break;
	//------------------
		case 'edit':

			$_SESSION['current'] = 'edit';
			header("Location: /");

		break;
	//------------------
		case 'projects':

			$_SESSION['current'] = 'Projects';
			header("Location: /");

		break;
	//------------------
		case 'list':

			if($_GET['deluser']){
				$_SESSION['deluser'] = $_GET['deluser'];
			}
			$_SESSION['current'] = 'list user';
			header("Location: /");

		break;
		
	}

	
	switch($perm->var){

		case 'Projects':

			$all = scandir('projects');
			unset($all[0]);
			unset($all[1]);
			unset($all[array_search('.htaccess', $all)]);

		break;
	//------------------
		case 'list user':

			if(isset($_SESSION['deluser'])){
				if($_SESSION['deluser'] != $_SESSION['system-user']){
					$perm->deleteUser($_SESSION['deluser']);
				}
				$_SESSION['deluser'] = null;
			}
			
			$users = $perm->getAllUser();

		break;
	//------------------
		case 'edit':

			if($_POST['newuser']){
				if($_POST['newuser'] AND $_POST['newpassword']){
					if(!in_array($_POST['newuser'], $perm->getAllUser())){
						$perm->createUser($_POST['newuser'], $_POST['newpassword']);
					}else{
						$newerror = true;
					}
				}else{
					$newerror = true;
				}
			}

		break;

	}
	
}else{
	
	if($_POST['user']){
		
		if($perm->login($_POST['user'], $_POST['password'])){
			$_SESSION['current'] = "Projects";
			header("Location: /");
		}else{
			$login_error = true;
		}

	}
	
}
?>
<!DOCTYPE html>
<html lang="de">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
		<title><?php echo $perm->var;?></title>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	<style>
		.card{border-top: 6px solid #6172D6;border-radius:0;}
		.stripe{border-top: 260px solid #3F51B5;}
		.container{margin-top:-110px;}
		.top{margin-top:-110px;}
		.right2{margin-right:70px !important;}
		.card-content p{padding:20px;}
		.card-content p a{color: #424242;}
		.card-content p:nth-child(1){padding-top: 30px;}
		.edit-button{display:block;margin-left:auto;margin-right:auto;}
		</style>
	</head>
	<body <?php if(@$newerror == true){echo 'onload="$(\'#addUser\').modal(\'open\');"';} ?>>
	<div class="stripe"></div>
	<div class="container">
	<?php if($perm->var != "Login"){
			if($perm->hasPerm($_SESSION['system-user'], "edit") AND $perm->var != "edit"){
	?>
		<a class="btn-floating btn-large waves-effect waves-light red top right right2 tooltipped" data-position="bottom" data-delay="0" data-tooltip="Edit" href="index.php?action=edit">
			<i class="material-icons">mode_edit</i>
		</a>
	<?php } 
			if($perm->var != "Projects"){
	?>
		<a class="btn-floating btn-large waves-effect waves-light red top left tooltipped" data-position="bottom" data-delay="0" data-tooltip="Back" href="index.php?action=projects">
			<i class="material-icons">fast_rewind</i>
		</a>
	<?php } ?>
		<a class="btn-floating btn-large waves-effect waves-light red top right tooltipped" data-position="bottom" data-delay="0" data-tooltip="logout" href="index.php?action=logout">
			<i class="material-icons">power_settings_new</i>
		</a>
	<?php } ?>
	 <div class="row">
        <div class="col offset-l3 l6 s12">
          <div class="card">
            <div class="card-content">
              <span class="card-title"><u><?php echo $perm->var;?></u></span>
								<div class="row">
									<div class="col s12">
									<?php if($perm->var == "Login"){ ?>
										<form method="POST">
											<div class="row">
											<?php if(@$login_error == true){ ?>
												<span class="new badge red left" data-badge-caption="Error! Your Username/Password is incorrect!"></span>
											<?php } ?>
												<div class="input-field col s12">
													<input id="user" name="user" type="text">
													<label for="user">Username</label>
												</div>
												<div class="input-field col s12">
													<input id="password" name="password" type="password">
													<label for="password">Password</label>
												</div>
												<div class="input-field col s12">
													<button class="btn waves-effect waves-light" type="submit">Submit
														<i class="material-icons right">send</i>
													</button>
												</div>
											</div>
										</form>
									<?php }else if($perm->var == "Projects"){ 

										foreach($all as $data) {
											if($perm->hasPage($_SESSION['system-user'], $data)){
												echo '<p><a href="projects/'.$data.'">'.$data.'</a></p>';
											}
										}

									}else if($perm->var == "edit"){ ?>
									
									<a class="btn waves-effect waves-light edit-button" href="index.php?action=list">list User</a>
									
									<br>

									<a class="btn waves-effect waves-light edit-button" href="#addUser">create User</a>
									<div id="addUser" class="modal">
											<div class="modal-content">
												<h4>Create User</h4>
												<div class="row">
											<?php if(@$newerror == true){ ?>
													<span class="new badge red left" data-badge-caption="Error! The Username/Password is invalid or already existing!"></span>
											<?php } ?>
												<form method="POST">
													<div class="input-field col s12">
														<input id="newuser" name="newuser" type="text">
														<label for="newuser">Username</label>
													</div>
													<div class="input-field col s12">
														<input id="newpassword" name="newpassword" type="password">
														<label for="newpassword">Password</label>
													</div>
													<div class="input-field col s12">
													<button class="btn waves-effect waves-light" type="submit">Create
														<i class="material-icons right">send</i>
													</button>
												</div>
												</form>
												</div>
											</div>
											<div class="modal-footer">
												<a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
											</div>
										
									</div>
          
									<?php }else if($perm->var == "list user"){ 
											echo '<ul class="collection with-header">';
											foreach($users as $user) {
												echo '<li class="collection-item"><div>'.$user.'<a onclick="del(\''.$user.'\')" class="secondary-content"><i class="material-icons">delete</i></a></div></li>';
											}
											echo '</ul>';
									?>
									
									<script>
									
									function del(user){
										var del = confirm("Do you really want to remove "+user+"?");
										if(del == true){
											location.href("index.php?action=list&deluser="+user);
										}
									}
									
									</script>
									
								<?php	} ?>
								</div>
							</div>
            </div>
          </div>
        </div>
      </div>
	  </div>
		<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="http://materializecss.com/bin/materialize.js"></script>
		<script src="http://materializecss.com/js/init.js"></script>
	</body>
</html>
